/*
 * minit.c
 * Copyright 2024, Gary Wong <gtw@gnu.org>
 *
 * minit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * minit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with minit.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <config.h>

#if HAVE_LINUX_CLOSE_RANGE_H
#include <linux/close_range.h>
#endif
#include <errno.h>
#include <limits.h>
#if HAVE_SYS_MOUNT_H
#include <sys/mount.h>
#endif
#if HAVE_SYS_REBOOT_H
#include <sys/reboot.h>
#endif
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#if HAVE_TTYENT_H
#include <ttyent.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_UTMPX_H
#include <utmpx.h>
#endif
#if HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif

#define MIN_PERIOD 3 /* interval (in seconds) between processes on a TTY */
#define SHUTDOWN_TIME 5 /* time to wait after SIGTERM and before SIGKILL */

struct tty {
    pid_t pid; /* PID of process we spawned on the TTY, -1 if none */
    time_t time; /* time we spawned previous process */
    char **argv; /* command line of child process */
    char *tty; /* TTY device, without leading "/dev/" */
    struct tty *next;
};

static struct tty *first;

static struct tty *cleanup( pid_t pid, int wterm, int wstatus ) {

    struct tty *t;

    if( pid < 0 )
	return NULL;
    
    for( t = first; t; t = t->next )
	if( pid == t->pid ) {
	    struct utmpx ut;
	    struct timeval tv;
	    
	    memset( &ut, 0, sizeof ut );
	    gettimeofday( &tv, NULL );
	    ut.ut_type = DEAD_PROCESS;
	    ut.ut_pid = pid;
	    strcpy( ut.ut_line, t->tty );
	    ut.ut_exit.e_termination = wterm;
	    ut.ut_exit.e_exit = wstatus;
	    ut.ut_tv.tv_sec = tv.tv_sec;
	    ut.ut_tv.tv_usec = tv.tv_usec;
	    pututxline( &ut );
	    updwtmpx( _PATH_WTMP, &ut );
    
	    t->pid = -1;
	    return t;
	}

    return NULL;
}

static void shutdown( int sig ) {

    const char *action = sig == SIGINT ? "restart" : "halt";
    pid_t pid;
    struct utmpx ut;
    struct timeval tv;
    int status;

    /* The last thing we can say to syslogd before we kill it. */
    syslog( LOG_INFO, "preparing to %s system: "
	    "terminating all processes...", action );
    
    kill( -1, SIGTERM );
    sleep( SHUTDOWN_TIME );
    kill( -1, SIGKILL );

    while( ( pid = wait( &status ) ) != -1 || errno != ECHILD )
	cleanup( pid, WIFSIGNALED( status ) ? WTERMSIG( status ) : 0,
		 WIFEXITED( status ) ? WEXITSTATUS( status ) : -1 );

    memset( &ut, 0, sizeof ut );
    gettimeofday( &tv, NULL );
    ut.ut_type = RUN_LVL;
    strcpy( ut.ut_line, "console" );
    strcpy( ut.ut_id, "~~" );
    strcpy( ut.ut_user, action );
    ut.ut_tv.tv_sec = tv.tv_sec;
    ut.ut_tv.tv_usec = tv.tv_usec;
    updwtmpx( _PATH_WTMP, &ut );

    /* The last thing we can say by ourselves before we close file
       descriptors and filesystems. */
    syslog( LOG_INFO, "%s.", action );

    endutxent();
    closelog();
#if HAVE_LINUX_CLOSE_RANGE_H
    close_range( 0, UINT_MAX, 0 );
#endif
    umount2( "/", MNT_DETACH );
    mount( "", "/", "", MS_REMOUNT | MS_RDONLY, NULL );
    sync();

#if HAVE_SYS_REBOOT_H
    reboot( sig == SIGINT ? RB_AUTOBOOT : RB_POWER_OFF );
#else
    for(;;)
	pause();
#endif    
}

static void start( struct tty *t ) {

    pid_t pid;

    if( ( pid = fork() ) ) {
	t->pid = pid;
	    
	if( pid < 0 )
	    syslog( LOG_ERR, "fork: %m" );

	time( &t->time );
    } else {
	struct utmpx ut;
	struct timeval tv;
	sigset_t sigset;
	
	memset( &ut, 0, sizeof ut );
	gettimeofday( &tv, NULL );
	ut.ut_type = INIT_PROCESS;
	ut.ut_pid = getpid();
	strcpy( ut.ut_line, t->tty );
	ut.ut_tv.tv_sec = tv.tv_sec;
	ut.ut_tv.tv_usec = tv.tv_usec;
	pututxline( &ut );

	if( time( NULL ) - t->time < MIN_PERIOD ) {
	    syslog( LOG_WARNING, "process %s spawning too fast, pausing...",
		    t->argv[ 0 ] );
	    sleep( MIN_PERIOD );
	}
	
	endutxent();
	setsid();
	
	sigemptyset( &sigset );
	sigprocmask( SIG_SETMASK, &sigset, NULL );
	
	execvp( t->argv[ 0 ], t->argv );
	syslog( LOG_ERR, "%s: %m", t->argv[ 0 ] );
	_exit( 1 );
    }
}

extern int main( int argc, char *argv[], char *envp[] ) {

    static const char *const rc[] = {
	"/etc/rc", NULL
    };
	
    int status;
    pid_t rcpid;
    struct ttyent *te;
    struct tty **last = &first, *t;
    sigset_t sigset;
    struct sigaction sa;
    struct utmpx ut;
    struct timeval tv;
    
    /* We're not ready for signals yet. */
    sigfillset( &sigset );
    sigprocmask( SIG_SETMASK, &sigset, NULL );

    if( getpid() != 1 ) {
	fprintf( stderr, "%s: must be PID 1\n", argv[ 0 ] );

	return 1;
    }
    
    setsid();
    (void) chdir( "/" );

#if HAVE_SYS_REBOOT_H
    reboot( RB_DISABLE_CAD );
#endif

    sa.sa_handler = shutdown;
    sigfillset( &sa.sa_mask );
    sa.sa_flags = 0;
    sigaction( SIGINT, &sa, NULL );
    sigaction( SIGTERM, &sa, NULL );
    
    rcpid = fork();

    sigemptyset( &sigset );
    sigprocmask( SIG_SETMASK, &sigset, NULL );
    
    if( !rcpid ) {
	execve( rc[ 0 ], (char * const*) rc, envp );
	_exit( 1 );
    }

    if( rcpid != -1 )
	/* Wait for ANY process, not just rcpid, because we might inherit
	   unexpected orphans and we don't want zombies. */
	while( wait( &status ) != rcpid )
	    ;

    sigfillset( &sigset );
    sigprocmask( SIG_SETMASK, &sigset, NULL );
    
    openlog( "init", LOG_CONS, LOG_DAEMON );
    
    memset( &ut, 0, sizeof ut );
    gettimeofday( &tv, NULL );
    ut.ut_type = BOOT_TIME;
    strcpy( ut.ut_line, "console" );
    strcpy( ut.ut_id, "~~" );
    strcpy( ut.ut_user, "reboot" );
    ut.ut_tv.tv_sec = tv.tv_sec;
    ut.ut_tv.tv_usec = tv.tv_usec;
    updwtmpx( _PATH_WTMP, &ut );
	     
    if( WIFSIGNALED( status ) )
	syslog( LOG_WARNING, "%s: %s", rc[ 0 ],
		strsignal( WTERMSIG( status ) ) );
    else if( WIFEXITED( status ) && WEXITSTATUS( status ) )
	syslog( LOG_WARNING, "%s exited with status %d", rc[ 0 ],
		WTERMSIG( status ) );

    if( setttyent() < 0 )
	syslog( LOG_ERR, _PATH_TTYS ": %m" );

    while( ( te = getttyent() ) ) {
	int teargc = 0;
	char *p;

	*last = t = malloc( sizeof *t );
	t->pid = -1;
	t->time = 0;
	t->argv = malloc( ( strlen( te->ty_getty ) + 1 ) *
			  sizeof (char *) );
	t->tty = strdup( te->ty_name );

	p = strtok( te->ty_getty, " \t" );
	t->argv[ 0 ] = p ? strdup( p ) : NULL;
	
	while( p ) {
	    p = strtok( NULL, " \t" );
	    t->argv[ ++teargc ] = p ? strdup( p ) : NULL;
	}

	t->argv = realloc( t->argv, ( teargc + 1 ) * sizeof (char *) );
	t->next = NULL;
	last = &t->next;
    }
    
    endttyent();

    if( !first ) {
	/* Nothing defined in /etc/ttys: make something up. */
	static const char *const default_argv[] = {
	    "getty", "console", NULL
	};
	
	first = malloc( sizeof *first );
	first->pid = -1;
	first->argv = (char **) default_argv;
	first->tty = "console";
	first->next = NULL;
    }

    for( t = first; t; t = t->next )
	start( t );
    
    sigemptyset( &sigset );
    sigprocmask( SIG_SETMASK, &sigset, NULL );
    
    for(;;) {
	pid_t pid;
	pid = wait( &status );

	if( ( t = cleanup( pid, WIFSIGNALED( status ) ? WTERMSIG( status ) : 0,
			   WIFEXITED( status ) ? WEXITSTATUS( status ) :
			   -1 ) ) )
	    start( t );
    }
}
