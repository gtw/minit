/*
 * reboot.c
 * Copyright 2024, Gary Wong <gtw@gnu.org>
 *
 * minit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * minit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with minit.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <config.h>

#include <getopt.h>
#include <signal.h>
#include <stdio.h>

extern int main( int argc, char *argv[] ) {

    for(;;) {
	static const struct option opts[] = {
	    { "help", no_argument, 0, 'h' },
	    { "version", no_argument, 0, 'v' },
	    { 0, 0, 0, 0 }
	};
	int optidx;
	int c = getopt_long( argc, argv, "hv", opts, &optidx );

	if( c < 0 )
	    break;
	else if( c == 'h' ) {
	    printf( "Usage: %s\n"
		    "Cleanly shut down and restart the system.\n", argv[ 0 ] );

	    return 0;
	} else if( c == 'v' ) {
	    puts( "minit version " VERSION );
	    
	    return 0;
	} else
	    return 1;
    }	
    
    signal( SIGTERM, SIG_IGN );
    
    if( kill( 1, SIGINT ) < 0 ) {
	perror( argv[ 0 ] );

	return 1;
    }

    return 0;
}
